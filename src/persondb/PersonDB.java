/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persondb;

import java.util.*;


public class PersonDB {
    static ArrayList<String> arr = new ArrayList<String>();
    
    public static void Load(){
        
    }
    
    static Scanner kb = new Scanner (System.in);
    private static String username;
    private static String password;
    
    public static void showLogin(){
        System.out.println("Please input username and password");
    }
    public static void inputLogin(){
        System.out.print("Username : "); username = kb.nextLine(); 
        System.out.print("Password : "); password = kb.nextLine();
    }

    private static boolean isUsernameCorrect(String username) {
        return true;
    }
    private static boolean isPasswordCorrect(String password) {
        return true;
    }

    private static void checkUser() {
        if (!isUsernameCorrect(username)){
            showUserIncorrect();
            inputLogin();
        }else{
            chackPassword();
        }
    }
    private static void chackPassword() {
        if (!isPasswordCorrect(password)){
            showPasswordIncorrect();
            inputLogin();
        }else {
            System.out.println("Success");
        }
    }
    
    public static void showUserIncorrect(){
        System.err.println("User is incorrect.\n" + "Please try again");
    }
    public static void showPasswordIncorrect(){
        System.err.println("Password is incorrect.\n" +"Please try again");
    }
    
    public static void main(String[] args) {
        showLogin();
        inputLogin();
        checkUser();
    }
    
}
class Person{        
    private String username;
    private String password;	
    private String firstname;
    private String lastname;
    private float weight;
    private float height;

    Person(String username,String password,String firstname,String lastname,float weight,float height){

        this.username=username;
        this.password=password;
        this.firstname=firstname;
        this.lastname=lastname;
        this.weight=weight;
        this.height=height;
    }
        
        public String getUsername(){
            return username;
        }
}